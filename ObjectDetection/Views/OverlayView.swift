// Copyright 2019 The TensorFlow Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import UIKit
import AVFoundation
import TensorFlowLite

protocol OverlayViewProtocol: AnyObject {
    func set(objectOverlay: ObjectOverlay)
}

/**
 This UIView draws overlay on a detected object.
 */
class OverlayView: UIView {
    var objectOverlays: [ObjectOverlay] = []
    weak var delegate: OverlayViewProtocol?
    override func draw(_ rect: CGRect) {
        for objectOverlay in objectOverlays {
            guard objectOverlay.confidenceValue > 95 else {
                return
            }
            guard objectOverlay.originalBorderRect.height < 650 else {
                return
            }
            delegate?.set(objectOverlay: objectOverlay)
        }
    }
}
