//
//  ClientProtocol.swift
//

import Alamofire

// MARK: - ClientProtocol
protocol ClientProtocol: AnyObject {
    var requests: [String: DataRequest] { get set }
    func request<T: Decodable>(from function: String, resource: URLRequestConvertible, result: @escaping ResultHandler<T>)
}

// MARK: - Default Implementation
extension ClientProtocol {
    func request<T: Decodable>(from function: String = #function, resource: URLRequestConvertible, result: @escaping ResultHandler<T>) {
        requests[function]?.suspend()
        requests[function] = HTTPService.request(resource: resource, result: result)
    }
}

protocol UploadImageClientProtocol: AnyObject {
    func uploadImage<T: Decodable>(from function: String, data: Data, name: String, fileName: String, mimeType: String, parameters: [String: Any], resource: URLRequest, result: @escaping ResultHandler<T>)
}

extension UploadImageClientProtocol {
    func uploadImage<T: Decodable>(from function: String = #function, data: Data, name: String, fileName: String, mimeType: String, parameters: [String: Any], resource: URLRequest, result: @escaping ResultHandler<T>) {
        HTTPService.upload(data: data, name: name, fileName: fileName, mimeType: mimeType, parameters: parameters, resource: resource, result: result)
    }
}
