//
//  HealClient.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Alamofire

//MARK: - Handle Heal Protocol
protocol HandleHealClientProtocol: AnyObject {
    func HandleHeal(data: Data, name: String, fileName: String, mimeType: String, parameters: [String: Any], resource: URLRequestConvertible, result: @escaping ResultHandler<HandleHealResponse>)
}
//MARK: - Handle Heal Client
final class HandleHealClient: BaseClient { }
extension HandleHealClient: HandleHealClientProtocol {
    func HandleHeal(data: Data, name: String, fileName: String, mimeType: String, parameters: [String : Any], resource: URLRequestConvertible, result: @escaping ResultHandler<HandleHealResponse>) {
        
    }
}
//MARK: - Paramaters
struct HandleHealParameters {
    var option: String
    var nin: String
    var index: String
    var country: String
    var strategyType: String
    var companyId: String
    var provider: String
    var auditId: String
}
extension HandleHealParameters: ParametersConvertible {
    var asParameters: Parameters {
        [
            "option": option,
            "nin": nin,
            "index": index,
            "country": country,
            "strategy_type": strategyType,
            "company_id": companyId,
            "provider": provider,
            "auditId": auditId
        ]
    }
}
//MARK: - Handle Heal Response
struct HandleHealResponseSample: Decodable {
    var strategyType: String?
    var index: Int = 0
}
extension HandleHealResponseSample {
    enum CodingKeys: String, CodingKey {
        case strategyType = "strategy_type"
        case index = "index"
    }
}
struct HandleHealResponseData: Decodable {
    var uuid: String?
    var type: String?
    var sample: HandleHealResponseSample?
    var userId: String?
    var status: String?
    var provider: String?
    var companyId: String?
    var createdAt: String?
    var updatedAt: String?
}
extension HandleHealResponseData {
    enum CodingKeys: String, CodingKey {
        case uuid = "uuid"
        case type = "type"
        case sample = "sample"
        case userId = "user_id"
        case status = "status"
        case provider = "provider"
        case companyId = "company_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
struct HandleHealResponse: Decodable {
    var code: Int = 0
    var resource: String?
    var data: HandleHealResponseData?
    var message: String?
}
extension HandleHealResponse {
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case resource = "resource"
        case data = "data"
        case message = "message"
    }
}

