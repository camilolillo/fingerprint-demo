//
//  GetTokenClient.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Alamofire

//MARK: - GetTokenClientProtocol
protocol GetTokenClientProtocol: AnyObject {
    func getToken(parameters: GetTokenParamaters, result: @escaping ResultHandler<GetTokenResponse>)
}
//MARK: - GetTokenClient
final class GetTokenClient: BaseClient { }
extension GetTokenClient: GetTokenClientProtocol {
    func getToken(parameters: GetTokenParamaters, result: @escaping ResultHandler<GetTokenResponse>) {
        request(resource: AteneaRouter.getToken(parameters: parameters), result: result)
    }
}
//MARK: - Get Token Paramaters
struct GetTokenParamaters {
    let clientId: String
    let clientSecret: String
    let grantType: String
}
extension GetTokenParamaters: ParametersConvertible {
    var asParameters: Parameters {
        [
            "client_id": clientId,
            "client_secret": clientSecret,
            "grant_type": grantType
        ]
    }
}
//MARK: - GetTokenResponse
struct GetTokenResponse: Codable {
    var accessToken: String?
    var tokenType: String?
    var expiresIn = 0
    var scope: String?
    var createdAt = 0
}
// MARK: - CodingKeys
extension GetTokenResponse {
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case scope = "scope"
        case createdAt = "created_at"
    }
}
