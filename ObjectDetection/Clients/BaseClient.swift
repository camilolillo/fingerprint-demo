//
//  BaseClient.swift
//

import Alamofire

// MARK: - BaseClient
class BaseClient {
    internal var requests = [String: DataRequest]() {
        didSet {
            requests.forEach { print("\(self).request: \($0)\n") }
        }
    }
}

// MARK: - ClientProtocol
extension BaseClient: ClientProtocol {}
