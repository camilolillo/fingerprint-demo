//
//  String+Constants.swift
//

import Foundation

// MARK: - Constants
extension String {
    static let empty = ""
    static let zero = "0"

    static let notAValidRUT = "No es un RUT válido"
    static let thereIsNoAvailableBrowser = "No hay un navegador disponible"
    static let noEmptyPassword = "Debe ingresar su contraseña para continuar"

    static let appLocale = "es_CL"
    static let appEn = "en"
    
    static let EEEddMMMyyyyHHmmsszzz = "EEE, dd MMM yyyy HH:mm:ss zzz"
    static let yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"
    static let ddMMyyyyHHmmss = "dd/MM/yyyy HH:mm:ss"
    static let ddMMMHHmm = "dd MMM / HH:mm"
    static let dMy = "d/M/y"
    
    static let defaultAcceptActionTitle = "Aceptar"
    static let defaultAlertTitle = "Atención"
    static let defaultWarningTitle = "ADVERTENCIA"
    static let defaultAlertMessage = "Ha ocurrido un error, vuelve a intentarlo"
    static let cancel = "Cancelar"
    static let ok = "OK"
    static let error = "ERROR"
    static let biometricFallbackTitle = "Por favor utilice la firma con contraseña."
    static let goBack = "Volver"
    
    static let emptyFields = "Campos vacíos"
    static let fillEmptyFields = "Debes completar los campos para continuar"
    static let passwordsMustMatch = "Las contraseñas ingresadas deben coincidir"
    static let failureRecoveringPassword = "Hubo un problema al tratar de recuperar la contraseña."
    static let failureGetUserData = "Hubo un problema al tratar de recuperar los datos de usuario."
    
    static let faqURLAsString = "https://identidaddigital.com/preguntas_frecuentes.html"
    static let cueInfoURLAsString = "https://claveunica.gob.cl/que-es"
    
    static let getCUEInfoURLAsString = "https://claveunica.gob.cl/"
    
    static let feaContractURLAsString = "https://identidaddigital.acepta.com/contrato_feam.pdf"

    static let termsAndConditionsTitle = "Términos y condiciones"
    static let termsAndConditionsFileName = "terminos_condiciones"
    
    static let privacyPoliciesTitle = "Políticas de privacidad"
    static let privacyPoliciesFileName = "politicas_privacidad"
    
    static let noBiometricCapability = "Dispositivo sin capacidad biométrica"
    
    static let reject = "Rechazar"
    static let rejectMessage = "¿Está seguro que desea rechazar este documento? Ya no podrá firmar ni visualizarlo nuevamente"
    
    static let symbolsSetAsString = "=$@ºª/&|'¡!%*()?&#,;:._-"
    static let passwordPattern = "\\b^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$\\b"
    
    static let passwordIndication = "Debe contener un mínimo de 8 carácteres, al menos una mayúscula, una minúscula, un número, y un símbolo"
    
    static let fea = "Firma Electrónica Avanzada"
}
