//
//  TypeAliases.swift
//

// MARK: -  Typealiases
typealias CompletionHandler = (() -> Void)?
typealias Handler<T> = ((T) -> Void)
typealias ResultHandler<T> = ((Result<T, Error>) -> Void)
typealias CustomErrorResultHandler<T, E: Error> = ((Result<T, E>) -> Void)
