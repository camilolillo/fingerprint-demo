//
//  DoneViewController.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 21-06-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import UIKit

enum FingerprintResult {
    case successfull
    case fail
}

class DoneViewController: UIViewController {
    
    lazy var bottomStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var mainStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 32
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var resultImage: UIImageView = {
        let view = UIImageView()
        if UserDefaults.standard.bool(forKey: "isSuccess") {
            view.image = UIImage(named: "success")
        } else {
            view.image = UIImage(named: "fail")
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 24)
        if UserDefaults.standard.bool(forKey: "isSuccess") {
            label.text = "¡Hemos validado tu cédula con éxito!"
        } else {
            label.text = "No hemos podido validar tu huella"
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16)
        if UserDefaults.standard.bool(forKey: "isSuccess") {
            label.text = "Ya solo falta que te tomes una selfie y podremos validar tu identidad."
        } else {
            label.text = "Recuerda que tienes solamente 3 intentos, de otro mode deberás esperar 10 minutos o bien ingresar por otro método."
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var mainButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 24
        button.layer.backgroundColor = UIColor(red: 0.11, green: 0.106, blue: 0.122, alpha: 1).cgColor
        if UserDefaults.standard.bool(forKey: "isSuccess") {
            button.setTitle("Continuar", for: .normal)
        } else {
            button.setTitle("Volver", for: .normal)
        }
        button.addTarget(self, action: #selector(onMainButtonPressed(sender:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var logoImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "logo_trust")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var logoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 10)
        label.text = "Powered by"
        label.textColor = UIColor(red: 0.565, green: 0.565, blue: 0.565, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        view.backgroundColor = .white
        
        logoImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        logoImage.widthAnchor.constraint(equalToConstant: 56).isActive = true
        
        bottomStack.addArrangedSubview(logoLabel)
        bottomStack.addArrangedSubview(logoImage)
        
        resultImage.heightAnchor.constraint(equalToConstant: 100).isActive = true
        resultImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        mainButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        mainStack.addArrangedSubview(resultImage)
        mainStack.addArrangedSubview(titleLabel)
        mainStack.addArrangedSubview(messageLabel)
        mainStack.addArrangedSubview(mainButton)
        
        view.addSubview(bottomStack)
        view.addSubview(mainStack)
        
        bottomStack.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32).isActive = true
        bottomStack.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        mainStack.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mainStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 32).isActive = true
        mainStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32).isActive = true
        
        mainButton.widthAnchor.constraint(equalTo: mainStack.widthAnchor).isActive = true
    }
    
    
    
    @objc func onMainButtonPressed(sender: UIButton) {
        navigationController?.dismiss(animated: true)
    }

}
