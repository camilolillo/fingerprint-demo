// Copyright 2019 The TensorFlow Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import UIKit
import AVKit
import Alamofire

struct ImageResponse: Decodable {
    let status: Int?
    let message: String?
}

class ViewController: UIViewController {
    
    // MARK: Storyboards Connections
    @IBOutlet weak var previewView: PreviewView!
    
    @IBOutlet weak var overlayView: OverlayView! {
        didSet {
            if let _ = overlayView {
                overlayView.delegate = self
            }
        }
    }
    
    @IBOutlet weak var fingerMask: UIView! {
        didSet {
            
            fingerMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            fingerMask.layer.borderWidth = 5
            fingerMask.layer.borderColor = UIColor.systemPink.cgColor
            
            fingerMask.layer.cornerRadius = fingerMask.frame.height/2
            
            fingerMask.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
            
        }
    }
    
    // MARK: Constants
    private let displayFont = UIFont.systemFont(ofSize: 14.0, weight: .medium)
    private let edgeOffset: CGFloat = 2.0
    private let labelOffset: CGFloat = 10.0
    private let animationDuration = 0.5
    private let collapseTransitionThreshold: CGFloat = -30.0
    private let expandTransitionThreshold: CGFloat = 30.0
    private let delayBetweenInferencesMs: Double = 200
    
    // MARK: Instance Variables
    private var initialBottomSpace: CGFloat = 0.0
    
    // Holds the results at any time
    private var result: TensorResult?
    private var previousInferenceTimeMs: TimeInterval = Date.distantPast.timeIntervalSince1970 * 1000
    
    // MARK: Controllers that manage functionality
    private lazy var cameraFeedManager = CameraFeedManager(previewView: previewView)
    
    private var modelDataHandler: ModelDataHandler? =
    ModelDataHandler(modelFileInfo: MobileNetSSD.modelInfo, labelsFileInfo: MobileNetSSD.labelsInfo)
    
    private var objectOverlay: ObjectOverlay? {
        didSet {
            if let _ = objectOverlay {
                cameraFeedManager.takePhoto()
            }
        }
    }
    
    private var image: UIImage? {
        didSet {
            if let overlay = objectOverlay, let image = image {
                let croppedImage = image.crop(with: overlay.originalBorderRect)
                let grayScaleImage = croppedImage.convertToGrayScale()
                let contrastedImage = grayScaleImage.processImage()
   
                print(contrastedImage.size)
                
            }
        }
    }
    
    // MARK: View Handling Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard modelDataHandler != nil else {
            fatalError("Failed to load model")
        }
        
        cameraFeedManager.delegate = self
        overlayView.clearsContextBeforeDrawing = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraFeedManager.checkCameraConfigurationAndStartSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        cameraFeedManager.stopSession()
    }
    
    //MARK: - Intent stack
    @IBOutlet weak var intentView01: UIView! {
        didSet {
            intentView01.isHidden = true
            intentView01.backgroundColor = .systemPink
            intentView01.clipsToBounds = true
            intentView01.layer.cornerRadius = intentView01.frame.size.height / 2.0
        }
    }
    
    @IBOutlet weak var intentView02: UIView! {
        didSet {
            intentView02.isHidden = true
            intentView02.backgroundColor = .systemPink
            intentView02.clipsToBounds = true
            intentView02.layer.cornerRadius = intentView02.frame.size.height / 2.0
        }
    }
    
    @IBOutlet weak var intentView03: UIView! {
        didSet {
            intentView03.isHidden = true
            intentView03.backgroundColor = .systemPink
            intentView03.clipsToBounds = true
            intentView03.layer.cornerRadius = intentView03.frame.size.height / 2.0
        }
    }
    

}

// MARK: CameraFeedManagerDelegate Methods
extension ViewController: CameraFeedManagerDelegate {
    func set(image: UIImage) {
        self.image = image
    }
    
    func didOutput(pixelBuffer: CVPixelBuffer) {
        runModel(onPixelBuffer: pixelBuffer)
    }
    
    // MARK: Session Handling Alerts
    func sessionRunTimeErrorOccurred() { }
    
    func sessionWasInterrupted(canResumeManually resumeManually: Bool) { }
    
    func sessionInterruptionEnded() { }
    
    func presentVideoConfigurationErrorAlert() {
        
        let alertController = UIAlertController(title: "Configuration Failed", message: "Configuration of camera has failed.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func presentCameraPermissionsDeniedAlert() {
        
        let alertController = UIAlertController(title: "Camera Permissions Denied", message: "Camera permissions have been denied for this app. You can change this by going to Settings", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    /** This method runs the live camera pixelBuffer through tensorFlow to get the result.
     */
    @objc func runModel(onPixelBuffer pixelBuffer: CVPixelBuffer) {
        
        // Run the live camera pixelBuffer through tensorFlow to get the result
        
        let currentTimeMs = Date().timeIntervalSince1970 * 1000
        
        guard  (currentTimeMs - previousInferenceTimeMs) >= delayBetweenInferencesMs else {
            return
        }
        
        previousInferenceTimeMs = currentTimeMs
        result = self.modelDataHandler?.runModel(onFrame: pixelBuffer)
        
        guard let displayResult = result else {
            return
        }
        
        let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
        
        DispatchQueue.main.async {
            self.drawAfterPerformingCalculations(onInferences: displayResult.inferences, withImageSize: CGSize(width: CGFloat(width), height: CGFloat(height)))
        }
    }
    
    /**
     This method takes the results, translates the bounding box rects to the current view, draws the bounding boxes, classNames and confidence scores of inferences.
     */
    func drawAfterPerformingCalculations(onInferences inferences: [Inference], withImageSize imageSize: CGSize) {
        
        guard !inferences.isEmpty else { return }
        
        for inference in inferences {

            var convertedRect = inference
                .rect
                .applying(
                    .init(
                        scaleX: self.overlayView.bounds.size.width / imageSize.width,
                        y: self.overlayView.bounds.size.height / imageSize.height)
                )
            
            if convertedRect.origin.x < 0 {
                convertedRect.origin.x = self.edgeOffset
            }
            
            if convertedRect.origin.y < 0 {
                convertedRect.origin.y = self.edgeOffset
            }
            
            if convertedRect.maxY > self.overlayView.bounds.maxY {
                convertedRect.size.height = self.overlayView.bounds.maxY - convertedRect.origin.y - self.edgeOffset
            }
            
            if convertedRect.maxX > self.overlayView.bounds.maxX {
                convertedRect.size.width = self.overlayView.bounds.maxX - convertedRect.origin.x - self.edgeOffset
            }

            let confidenceValue = Int(inference.confidence * 100.0)

            guard inference.className == "Good_Phalange" else {
                return
            }
            guard confidenceValue > 95 else {
                return
            }
            guard inference.rect.height < 650 else {
                return
            }
            guard convertedRect.origin.x > 74, convertedRect.origin.x < 86, convertedRect.origin.y > 134, convertedRect.origin.y < 146 else {
                self.fingerMask.layer.borderColor = UIColor.systemPink.cgColor
                return
            }
            self.fingerMask.layer.borderColor = UIColor.green.cgColor
            self.cameraFeedManager.takePhoto()
        }
    }
}

extension ViewController: OverlayViewProtocol {
    func set(objectOverlay: ObjectOverlay) {
        
        let fingerX = objectOverlay.borderRect.origin.x
        let fingerY = objectOverlay.borderRect.origin.y
        
        guard fingerX > 74, fingerX < 86, fingerY > 134, fingerY < 146 else {
            self.fingerMask.layer.borderColor = UIColor.systemPink.cgColor
            return
        }
    
        self.fingerMask.layer.borderColor = UIColor.green.cgColor
        self.objectOverlay = objectOverlay
    }
}


