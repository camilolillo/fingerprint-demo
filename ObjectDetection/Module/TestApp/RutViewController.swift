//
//  RutViewController.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import UIKit

class RutViewController: UIViewController {

    @IBOutlet weak var rutField: UITextField!
    
    @IBOutlet weak var validateButton: UIButton! {
        didSet {
            validateButton.backgroundColor = UIColor(red: 0, green: 0.851, blue: 0.906, alpha: 1)
            validateButton.layer.cornerRadius = 24
        }
    }
    
    @IBOutlet weak var enrollButton: UIButton! {
        didSet {
            enrollButton.backgroundColor = UIColor(red: 0.11, green: 0.106, blue: 0.122, alpha: 1)
            enrollButton.layer.cornerRadius = 24
        }
    }
    
    @IBAction func enrollButton(_ sender: Any) {
        guard let rut = rutField.text, rut.count > 0 else {
            return
        }
        UserDefaults.standard.set(rut, forKey: "rut")
        UserDefaults.standard.set("enroll", forKey: "option")
        let vc = CameraViewController.storyboardViewController()
        let nc = UINavigationController(rootViewController: vc)
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true)
    }
    
    @IBAction func validateButton(_ sender: Any) {
        guard let rut = rutField.text, rut.count > 0  else {
            return
        }
        UserDefaults.standard.set(rut, forKey: "rut")
        UserDefaults.standard.set("validate", forKey: "option")
        let vc = CameraViewController.storyboardViewController()
        let nc = UINavigationController(rootViewController: vc)
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true)
    }

}
