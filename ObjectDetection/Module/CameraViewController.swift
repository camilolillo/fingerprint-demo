//
//  CameraViewController.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import UIKit
import Alamofire
import MetalPetal

class CameraViewController: UIViewController {
    
    // MARK: Storyboards Connections
    @IBOutlet weak var instructionsViewContainer: UIView!
    @IBOutlet weak var instructionsView: UIView!  {
        didSet {
            instructionsView.layer.cornerRadius = 16
        }
    }
    @IBAction func confirmButton(_ sender: Any) {
        instructionsViewContainer.isHidden = true
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            cancelButton.addTarget(
                self, action: #selector(onBackButtonPressed(sender:)), for: .touchUpInside
            )
        }
    }
    
    @IBOutlet weak var previewView: PreviewView!
    
    @IBOutlet weak var overlayView: OverlayView! {
        didSet {
            if let _ = overlayView {
                overlayView.delegate = self
            }
        }
    }
    
    @IBOutlet weak var fingerMask: UIView! {
        didSet {
            fingerMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
//            fingerMask.layer.borderWidth = 5
//            fingerMask.layer.borderColor = UIColor.systemPink.cgColor
//            fingerMask.layer.cornerRadius = fingerMask.frame.height/2
//            fingerMask.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        }
    }
    
    @IBOutlet weak var fingerMaskImage: UIImageView!
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            backButton.addTarget(
                self,
                action: #selector(onBackButtonPressed(sender:)),
                for: .touchUpInside
            )
        }
    }
    
//    // MARK: Constants
//    private let displayFont = UIFont.systemFont(ofSize: 14.0, weight: .medium)
    private let edgeOffset: CGFloat = 2.0
//    private let labelOffset: CGFloat = 10.0
//    private let animationDuration = 0.5
//    private let collapseTransitionThreshold: CGFloat = -30.0
//    private let expandTransitionThreshold: CGFloat = 30.0
    private let delayBetweenInferencesMs: Double = 200
//
//    // MARK: Instance Variables
//    private var initialBottomSpace: CGFloat = 0.0
    
    // Holds the results at any time
    private var result: TensorResult?
    private var previousInferenceTimeMs: TimeInterval = Date.distantPast.timeIntervalSince1970 * 1000
    
    // MARK: Controllers that manage functionality
    private lazy var cameraFeedManager = CameraFeedManager(previewView: previewView)
    
    private var modelDataHandler: ModelDataHandler? =
    ModelDataHandler(modelFileInfo: MobileNetSSD.modelInfo, labelsFileInfo: MobileNetSSD.labelsInfo)
    
    private var objectOverlay: ObjectOverlay?
    
    private var intents = 0
    
    private var image: UIImage? {
        didSet {
            if let overlay = objectOverlay, let image = image {
                //Cropping
                let croppedImage = image.crop(with: overlay.originalBorderRect)
                //Rotating
                guard let rotatedImage = croppedImage.rotate(radians: .pi/2) else {
                    return
                }
                //Set image to cgImage to use with Metal Petal
                guard let rotatedCIImage = CIImage(image: rotatedImage) else {
                    return
                }
                
                //Applying Metal Petal CLAHE filter
                let inputImage = MTIImage(ciImage: rotatedCIImage).unpremultiplyingAlpha()
                let filter = MTICLAHEFilter()
                filter.inputImage = inputImage
                guard let outputImage = filter.outputImage else {
                    return
                }
                //Transform MTIImage to UIImage to send as PNG
                var imageToMask = UIImage()
                guard let device = MTLCreateSystemDefaultDevice() else {
                    return
                }
                do {
                    let context = try MTIContext(device: device)
                    let filteredImage = try context.makeCGImage(from: outputImage)
                    let finalImage = UIImage(cgImage: filteredImage)
                    imageToMask = finalImage
                } catch {
                    print(error)
                }
                //Cropping
                let newCroppedImage = crop(sourceImage: imageToMask)
                //Resizing image
                ///GetMask
                guard let mask = UIImage(named: "mask.png") else {
                    return
                }
                let size = CGSize.init(width: 256, height: 360)
                guard let firstResizedImage = resizedImage(image: newCroppedImage, for: size) else {
                    return
                }
                
                let grayScaleImage = firstResizedImage.convertToGrayScale()
                //let contrastedImage = grayScaleImage.processImage()
                
                //Masking image
                let maskedImage = grayScaleImage.applyImageMask(mask: mask)
                
                guard let finalImage = maskedImage.resize(to: CGSize.init(width: 256, height: 360)) else {
                    return
                }
                
                if
                    let option = UserDefaults.standard.string(forKey: "option"),
                    let nin = UserDefaults.standard.string(forKey: "rut")?.replacingOccurrences(of: "-", with: ""),
                    let imageData = finalImage.pngData() {
                    
                    let parameters = HandleHealParameters(
                        option: option,
                        nin: nin,
                        index: "3",
                        country: "CHL",
                        strategyType: "finger",
                        companyId: "1",
                        provider: "TRUST",
                        auditId: "lasjdihfjadf-ajsdhfkjlhalsdf-kjakhsdlfkjas"
                    )
                    
                    HTTPService.upload(data: imageData, name: "sample", fileName: "fingerprint", mimeType: "image/png", parameters: parameters.asParameters, resource: LibraryRouter.handleHeal(parameters: parameters)) {
                        (result: Result<HandleHealResponse, Error>) in
                        switch result {
                        case .success(let response):
                            
                            guard response.data?.status != "failure" else {
                                self.handleError()
                                return
                            }
                            
                            guard response.message == "OK" else {
                                self.handleError()
                                return
                            }
                            
                            UserDefaults.standard.set(true, forKey: "isSuccess")
                            
                            let vc = DoneViewController()
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                            
                        case .failure(let error):
                            self.handleError()
                            print(error)
                        }
                    }
                }
            }
        }
    }
    
    private let dialogView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 12
        view.layer.zPosition = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Toma una fotografía de tu huella digital"
        return label
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16)
        label.text = "Mantén tu dedo dentro del límite hasta que tomemos la fotografía de las huellas digitales que tengas habilitadas. Esto es para verificar que eres la persona quien dices ser."
        return label
    }()
    
    private let intentLabel: UILabel = {
        let label = UILabel()
        label.text = "Tienes 3 intentos"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor(red: 0.565, green: 0.565, blue: 0.565, alpha: 1)
        return label
    }()
    
    private let infoStackView: UIStackView = {
        let stack = UIStackView()
        stack.contentMode = .center
        stack.axis = .vertical
        stack.spacing = 32
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private let intentStackView: UIStackView = {
        let stack = UIStackView()
        stack.contentMode = .center
        stack.alignment = .center
        stack.axis = .vertical
        stack.spacing = 16
        return stack
    }()
    
    private let dotsStackView: UIStackView = {
        let stack = UIStackView()
        stack.contentMode = .center
        stack.distribution = .equalCentering
        stack.axis = .horizontal
        stack.spacing = 16
        return stack
    }()
    
    private let dot01: UIView = {
        let view = UIView()
        view.layer.cornerRadius = view.frame.size.width/2
        view.backgroundColor = UIColor(red: 0.565, green: 0.565, blue: 0.565, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let dot02: UIView = {
        let view = UIView()
        view.layer.cornerRadius = view.frame.size.width/2
        view.backgroundColor = UIColor(red: 0.565, green: 0.565, blue: 0.565, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let dot03: UIView = {
        let view = UIView()
        view.layer.cornerRadius = view.frame.size.width/2
        view.backgroundColor = UIColor(red: 0.565, green: 0.565, blue: 0.565, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: View Handling Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        overlayView.addSubview(dialogView)
        infoStackView.addArrangedSubview(titleLabel)
        infoStackView.addArrangedSubview(messageLabel)
        intentStackView.addArrangedSubview(intentLabel)
        dotsStackView.addArrangedSubview(dot01)
        dotsStackView.addArrangedSubview(dot02)
        dotsStackView.addArrangedSubview(dot03)
        intentStackView.addArrangedSubview(dotsStackView)
        infoStackView.addArrangedSubview(intentStackView)
        dialogView.addSubview(infoStackView)
        
        dialogView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32).isActive = true
        dialogView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        dialogView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        
        infoStackView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -32).isActive = true
        infoStackView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 32).isActive = true
        infoStackView.topAnchor.constraint(equalTo: dialogView.topAnchor, constant: 32).isActive = true
        infoStackView.bottomAnchor.constraint(equalTo: dialogView.bottomAnchor, constant: -32).isActive = true
        
        dot01.heightAnchor.constraint(equalToConstant: 8).isActive = true
        dot01.widthAnchor.constraint(equalToConstant: 8).isActive = true
        dot02.heightAnchor.constraint(equalToConstant: 8).isActive = true
        dot02.widthAnchor.constraint(equalToConstant: 8).isActive = true
        dot03.heightAnchor.constraint(equalToConstant: 8).isActive = true
        dot03.widthAnchor.constraint(equalToConstant: 8).isActive = true
        
        guard modelDataHandler != nil else {
            fatalError("Failed to load model")
        }
        
        cameraFeedManager.delegate = self
        overlayView.clearsContextBeforeDrawing = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraFeedManager.checkCameraConfigurationAndStartSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        cameraFeedManager.stopSession()
    }
    
    //MARK: - Intent stack
    @IBOutlet weak var intentView01: UIView! {
        didSet {
            intentView01.isHidden = true
            intentView01.backgroundColor = .systemPink
            intentView01.clipsToBounds = true
            intentView01.layer.cornerRadius = intentView01.frame.size.height / 2.0
        }
    }
    
    @IBOutlet weak var intentView02: UIView! {
        didSet {
            intentView02.isHidden = true
            intentView02.backgroundColor = .systemPink
            intentView02.clipsToBounds = true
            intentView02.layer.cornerRadius = intentView02.frame.size.height / 2.0
        }
    }
    
    @IBOutlet weak var intentView03: UIView! {
        didSet {
            intentView03.isHidden = true
            intentView03.backgroundColor = .systemPink
            intentView03.clipsToBounds = true
            intentView03.layer.cornerRadius = intentView03.frame.size.height / 2.0
        }
    }
    
    func handleError() {
        
        self.intents = self.intents+1
        
        guard intents < 4 else {
            let vc = DoneViewController()
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        switch intents {
        case 1:
            dot01.backgroundColor = .red
        case 2:
            dot02.backgroundColor = .red
        case 3:
            dot03.backgroundColor = .red
        default:
            let vc = DoneViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
        
        cameraFeedManager.resumeInterruptedSession() { _ in }
        
    }
    
    func alertAction(action: UIAlertAction) {
        dismiss(animated: true)
    }
    
    func cropImage(_ inputImage: UIImage, toRect cropRect: CGRect, viewWidth: CGFloat, viewHeight: CGFloat) -> UIImage? {
        
        let imageViewScale = max(inputImage.size.width / viewWidth,
                                 inputImage.size.height / viewHeight)


        // Scale cropRect to handle images larger than shown-on-screen size
        let cropZone = CGRect(x:cropRect.origin.x * imageViewScale,
                              y:cropRect.origin.y * imageViewScale,
                              width:cropRect.size.width * imageViewScale,
                              height:cropRect.size.height * imageViewScale)


        // Perform cropping in Core Graphics
        guard let cutImageRef: CGImage = inputImage.cgImage?.cropping(to:cropZone)
        else {
            return nil
        }


        // Return image to UIImage
        let croppedImage: UIImage = UIImage(cgImage: cutImageRef)
        return croppedImage
    }
    
    // Technique #1
    func resizedImage(image: UIImage, for size: CGSize) -> UIImage? {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { (context) in
            image.draw(in: CGRect(origin: .zero, size: size))
        }
    }
    
    func crop(sourceImage: UIImage) -> UIImage {

        // The shortest side
        let sideLength = min(
            sourceImage.size.width,
            sourceImage.size.height
        )

        // Determines the x,y coordinate of a centered
        // sideLength by sideLength square
        let sourceSize = sourceImage.size
        let xOffset = (sourceSize.width - sideLength) / 2.0
        let yOffset = (sourceSize.height - sideLength) / 2.0

        // The cropRect is the rect of the image to keep,
        // in this case centered
        let cropRect = CGRect(
            x: 100,
            y: yOffset,
            width: (sourceSize.width-120),
            height: sideLength
        ).integral

        // Center crop the image
        let sourceCGImage = sourceImage.cgImage!
        let croppedCGImage = sourceCGImage.cropping(
            to: cropRect
        )!
        
        return UIImage(cgImage: croppedCGImage)
        
    }
}

// MARK: CameraFeedManagerDelegate Methods
extension CameraViewController: CameraFeedManagerDelegate {
    func set(image: UIImage) {
        self.image = image
    }
    
    func didOutput(pixelBuffer: CVPixelBuffer) {
        runModel(onPixelBuffer: pixelBuffer)
    }
    
    // MARK: Session Handling Alerts
    func sessionRunTimeErrorOccurred() { }
    
    func sessionWasInterrupted(canResumeManually resumeManually: Bool) { }
    
    func sessionInterruptionEnded() { }
    
    func presentVideoConfigurationErrorAlert() {
        
        let alertController = UIAlertController(title: "Configuration Failed", message: "Configuration of camera has failed.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func presentCameraPermissionsDeniedAlert() {
        
        let alertController = UIAlertController(title: "Camera Permissions Denied", message: "Camera permissions have been denied for this app. You can change this by going to Settings", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    /** This method runs the live camera pixelBuffer through tensorFlow to get the result.
     */
    @objc func runModel(onPixelBuffer pixelBuffer: CVPixelBuffer) {
        
        // Run the live camera pixelBuffer through tensorFlow to get the result
        
        let currentTimeMs = Date().timeIntervalSince1970 * 1000
        
        guard  (currentTimeMs - previousInferenceTimeMs) >= delayBetweenInferencesMs else {
            return
        }
        
        previousInferenceTimeMs = currentTimeMs
        result = self.modelDataHandler?.runModel(onFrame: pixelBuffer)
        
        guard let displayResult = result else {
            return
        }
        
        let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
        
        DispatchQueue.main.async {
            self.drawAfterPerformingCalculations(onInferences: displayResult.inferences, withImageSize: CGSize(width: CGFloat(width), height: CGFloat(height)))
        }
    }
    
    /**
     This method takes the results, translates the bounding box rects to the current view, draws the bounding boxes, classNames and confidence scores of inferences.
     */
    func drawAfterPerformingCalculations(onInferences inferences: [Inference], withImageSize imageSize: CGSize) {
        
        guard !inferences.isEmpty else { return }
        
        for inference in inferences {

            var convertedRect = inference
                .rect
                .applying(
                    .init(
                        scaleX: self.overlayView.bounds.size.width / imageSize.width,
                        y: self.overlayView.bounds.size.height / imageSize.height)
                )
            
            if convertedRect.origin.x < 0 {
                convertedRect.origin.x = self.edgeOffset
            }
            
            if convertedRect.origin.y < 0 {
                convertedRect.origin.y = self.edgeOffset
            }
            
            if convertedRect.maxY > self.overlayView.bounds.maxY {
                convertedRect.size.height = self.overlayView.bounds.maxY - convertedRect.origin.y - self.edgeOffset
            }
            
            if convertedRect.maxX > self.overlayView.bounds.maxX {
                convertedRect.size.width = self.overlayView.bounds.maxX - convertedRect.origin.x - self.edgeOffset
            }

            let confidenceValue = Int(inference.confidence * 100.0)

            guard inference.className == "Good_Phalange" else {
                return
            }
            guard confidenceValue > 95 else {
                return
            }
            guard inference.rect.height < 650 else {
                return
            }
            guard convertedRect.origin.x > 74, convertedRect.origin.x < 86, convertedRect.origin.y > 134, convertedRect.origin.y < 146 else {
                self.fingerMask.layer.borderColor = UIColor.systemPink.cgColor
                return
            }
            let objectOverlay = ObjectOverlay(
                originalBorderRect: inference.rect,
                borderRect: convertedRect,
                color: inference.displayColor,
                font: .systemFont(ofSize: 8),
                confidenceValue: confidenceValue
            )
            self.objectOverlay = objectOverlay
            self.fingerMaskImage.tintColor = .systemGreen
            self.cameraFeedManager.takePhoto()
            
        }
    }
    
    @objc func onBackButtonPressed(sender: UIButton) {
        cameraFeedManager.stopSession()
        dismiss(animated: true)
    }
}

extension CameraViewController: OverlayViewProtocol {
    func set(objectOverlay: ObjectOverlay) {
        
        let fingerX = objectOverlay.borderRect.origin.x
        let fingerY = objectOverlay.borderRect.origin.y
        
        guard fingerX > 74, fingerX < 86, fingerY > 134, fingerY < 146 else {
            self.fingerMask.layer.borderColor = UIColor.systemPink.cgColor
            return
        }
    
        self.fingerMask.layer.borderColor = UIColor.green.cgColor
        self.objectOverlay = objectOverlay
    }
}
