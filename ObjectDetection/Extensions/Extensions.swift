//
//  Utils.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 23-03-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func crop(with rect: CGRect) -> UIImage {
        let path = UIBezierPath(rect: rect)
        
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()

        // Set the clipping mask
        path.addClip()
        
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))

        guard let maskedImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }

        // Restore previous drawing context
        context.restoreGState()
        UIGraphicsEndImageContext()
        
        let croppedImage = UIImage(cgImage: maskedImage.cgImage!.cropping(to: path.bounds)!)
        
        return croppedImage
    }
    
    func convertToGrayScale() -> UIImage {

        // Create image rectangle with current image width/height
        let imageRect:CGRect = CGRect(x:0, y:0, width:self.size.width, height: self.size.height)

        // Grayscale color space
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let width = self.size.width
        let height = self.size.height

        // Create bitmap content with current image size and grayscale colorspace
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)

        // Draw image into current context, with specified rectangle
        // using previously defined context (with grayscale colorspace)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context?.draw(self.cgImage!, in: imageRect)
        let imageRef = context!.makeImage()

        // Create a new UIImage object
        let newImage = UIImage(cgImage: imageRef!)

        return newImage
    }
    
    func processImage() -> UIImage {
        
        guard let inputImage = CIImage(image: self) else {
            print("Extension UIImage - \(#function): Failed to load CIImage")
            return self
        }
        
        let exposureParameters = ["inputEV": 2.3]
        
        let contrastParameters = ["inputContrast": 1.2, "inputBrightness": 0]
        
        let outputImage = inputImage.applyingFilter("CIColorControls", parameters: contrastParameters).applyingFilter("CIExposureAdjust", parameters: exposureParameters)
        
//        let outputImage = inputImage.applyingFilter("CIColorControls", parameters: contrastParameters)
        
        let context = CIContext(options: nil)
        
        guard let img = context.createCGImage(outputImage, from: outputImage.extent) else {
            print("Extension UIImage - \(#function): Failed to load cgImage")
            return self
        }
        
        return UIImage(cgImage: img)
    }
    
//    func invertColors() -> UIImage {
//        guard let inputImage = CIImage(image: self) else {
//            print("Extension UIImage - \(#function): Failed to load CIImage")
//            return self
//        }
//        
//        let outputImage = inputImage.applyingFilter("CIColorInvert")
//        
//        let context = CIContext(options: nil)
//        
//        guard let img = context.createCGImage(outputImage, from: outputImage.extent) else {
//            print("Extension UIImage - \(#function): Failed to load cgImage")
//            return self
//        }
//        
//        return UIImage(cgImage: img)
//    }
    
    func increaseContrast() -> UIImage {
        let inputImage = CIImage(image: self)!
        let parameters = [
            "inputContrast": 1, "inputBrightness": 0.005
        ]
        let outputImage = inputImage.applyingFilter("CIColorControls", parameters: parameters)

        let context = CIContext(options: nil)
        let img = context.createCGImage(outputImage, from: outputImage.extent)!
        return UIImage(cgImage: img)
    }
    
    func invertColors() -> UIImage {

        let filter = CIFilter(name: "CIColorInvert") //this creates a CIFilter with the attribute color invert

        filter?.setValue(CIImage(image: self), forKey: kCIInputImageKey) //this applies our filter to our UIImage

        let newImage = UIImage(ciImage: (filter?.outputImage)!) //this takes our inverted image and stores it as a new UIImage
        
        return newImage
    }
    
    func applyImageMask(mask: UIImage) -> UIImage {
        let imageRenderer = UIGraphicsImageRenderer(size: mask.size)

        let maskedImage = imageRenderer.image { (ctx) in
            let cgContext = ctx.cgContext
            
            // draw original image
            let rect = CGRect(origin: .zero, size: self.size)
            self.draw(in: rect)

            // save state to restore the clipping mask
            cgContext.saveGState()

            // apply clipping mask accounting for CGImages drawn mirrored
            cgContext.translateBy(x: 0, y: rect.size.height)
            cgContext.scaleBy(x: 1.0, y: -1.0);
            cgContext.clip(to: rect, mask: mask.cgImage!)

            // fill with mask applied 
            cgContext.setBlendMode(.darken)
            UIColor.black.setFill()
            cgContext.fill(rect)

            // remove clipping mask
            cgContext.restoreGState()
        }
        return maskedImage
    }
    
    func rotate(radians: Float) -> UIImage? {
            var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
            // Trim off the extremely small float value to prevent core graphics from rounding it up
            newSize.width = floor(newSize.width)
            newSize.height = floor(newSize.height)

            UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
            let context = UIGraphicsGetCurrentContext()!

            // Move origin to middle
            context.translateBy(x: newSize.width/2, y: newSize.height/2)
            // Rotate around middle
            context.rotate(by: CGFloat(radians))
            // Draw the image at its center
            self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return newImage
        }
    
    func resizeWith(newSize: CGSize) -> UIImage {
        let image = UIGraphicsImageRenderer(size: newSize).image { _ in
            draw(in: CGRect(origin: .zero, size: newSize))
        }
            
        return image.withRenderingMode(renderingMode)
    }
    
    func scalePreservingAspectRatio(targetSize: CGSize) -> UIImage {
        // Determine the scale factor that preserves aspect ratio
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        let scaleFactor = min(widthRatio, heightRatio)
        
        // Compute the new image size that preserves aspect ratio
        let scaledImageSize = CGSize(
            width: size.width * scaleFactor,
            height: size.height * scaleFactor
        )

        // Draw and return the resized UIImage
        let renderer = UIGraphicsImageRenderer(
            size: scaledImageSize
        )

        let scaledImage = renderer.image { _ in
            self.draw(in: CGRect(
                origin: .zero,
                size: scaledImageSize
            ))
        }
        
        return scaledImage
    }
    
    func resize(to size:CGSize) -> UIImage? {
            
            let cgImage = self.cgImage!
            let destWidth = Int(size.width)
            let destHeight = Int(size.height)
            let bitsPerComponent = 8
            let bytesPerPixel = cgImage.bitsPerPixel / bitsPerComponent
            let destBytesPerRow = destWidth * bytesPerPixel
            
            let context = CGContext(data: nil,
                                    width: destWidth,
                                    height: destHeight,
                                    bitsPerComponent: bitsPerComponent,
                                    bytesPerRow: destBytesPerRow,
                                    space: cgImage.colorSpace!,
                                    bitmapInfo: cgImage.bitmapInfo.rawValue)!
            context.interpolationQuality = .high
            context.draw(cgImage, in: CGRect(origin: CGPoint.zero, size: size))
            return context.makeImage().flatMap { UIImage(cgImage: $0) }
        }
    
}

extension UIView {
    func transformToUIImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: self.bounds.size)
        let image = renderer.image { ctx in
            self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        }
        return image
    }
}
