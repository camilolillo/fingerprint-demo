//
//  HTTPService.swift
//

import Alamofire

// MARK: - HTTPService
enum HTTPService {}

extension HTTPService {
    @discardableResult static func request<T: Decodable>(resource: URLRequestConvertible, result: @escaping ResultHandler<T>) -> DataRequest {
        var jsonDecoder: JSONDecoder { .init() }
        return AF
            .request(resource)
//            .responseString { print("\n\(self).\(#function).responseString: \($0)") }
            .responseDecodable(decoder: jsonDecoder) { (response: AFDataResponse<T>) in

                switch response.result {
                case .success(let decodedObject):
                    print("\(self).\(#function).decodedObject: \(decodedObject)\n")
                    result(.success(decodedObject))
                case .failure(let error):
                    print("\(self).\(#function).error: \(error)")
                    print("\(self).\(#function).error.localizedDescription: \(error.localizedDescription)\n")
                    result(.failure(error))
                }
            }
    }
    
    @discardableResult static func upload<T: Decodable>(data: Data, name: String, fileName: String, mimeType: String, parameters: [String: Any], resource: URLRequestConvertible, result: @escaping ResultHandler<T>) -> DataRequest {
                
            var jsonDecoder: JSONDecoder {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                return jsonDecoder
            }

            return AF.upload(multipartFormData: {
                
                multipartFormData in
                
                multipartFormData.append(
                    data,
                    withName: name,
                    fileName: fileName,
                    mimeType: mimeType
                )
                
                parameters.map {
                    ($0, "\($1)".data(using: .utf8))
                }.filter {
                    $1 != nil
                }.forEach {
                    multipartFormData.append($1!, withName: $0)
                }
                
            }, with: resource).responseDecodable(decoder: jsonDecoder) {
                (response: AFDataResponse<T>) in
                    
                    switch response.result {
                    case .success(let decodedObject):
                        print("\(self).\(#function).decodedObject: \(decodedObject)\n")
                        result(.success(decodedObject))
                    case .failure(let error):
                        print("\(self).\(#function).error: \(error)")
                        print("\(self).\(#function).error.localizedDescription: \(error.localizedDescription)\n")
                        result(.failure(error))
                }
            }
        }
    
}

//// MARK: - Status code handling
//extension HTTPService {
//    private static var appCoordinator: AppCoordinator? { (UIApplication.shared.delegate as? AppDelegate)?.appCoordinator }
//
//    private static func handle(statusCode: Int, handler: CompletionHandler) {
//        print("\(self).\(#function).statusCode: \(statusCode)\n")
//        switch statusCode {
//        case 401:
//            OAuth2ClientManager.shared.authorize { (_, error) in
//                error == nil ? handler?() : handleExpiredSession()
//            }
//        default: break
//        }
//    }
//
//    private static func handleExpiredSession() {
//        appCoordinator?.handleExpiredSession()
//    }
//}
