//
//  AteneaEnvironment.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Foundation

enum AteneaEnvironment {
    case prod
}

extension AteneaEnvironment {
    static var current: AteneaEnvironment = .prod
    static var baseURLAsString: String {
        switch current {
        case .prod:
            return "https://atenea.trust.lat"
        }
    }
    static var baseURL: URL? { .init(string: baseURLAsString) }
}

extension AteneaEnvironment {
    static var clientId: String {
        switch current {
        case .prod:
            return "560fc380-321e-441f-bc96-18b4508087f0"
        }
    }
    
    static var cliendSecret: String {
        switch current {
        case .prod:
            return "9e2ed898-7a3a-4dbe-9d12-03485914c8ca"
        }
    }
}
