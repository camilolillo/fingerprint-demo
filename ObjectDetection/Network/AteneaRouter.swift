//
//  AteneaRouter.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Alamofire

enum AteneaRouter {
    case getToken(parameters: ParametersConvertible)
}
// MARK: - Endpoint
extension AteneaRouter: Endpoint {
    
    var baseURL: URL? { AteneaEnvironment.baseURL }
    
    var servicePath: String {
        switch self {
        case .getToken:
            return "/oauth"
        }
    }
    
    var path: String {
        switch self {
        case .getToken:
            return "/token/"
        }
    }
    
    var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        switch self {
        case .getToken:
            headers.add(.init(name: "Content-Type", value: "application/json"))
        }
        return headers
    }
    
    var method: HTTPMethod {
        switch self {
        case .getToken:
            return .post
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getToken(let parameters):
            return parameters.asParameters
        }
    }
}
// MARK: - URLRequestConvertible
extension AteneaRouter: URLRequestConvertible {}
