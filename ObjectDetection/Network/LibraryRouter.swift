//
//  LibraryRouter.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Alamofire

import Alamofire
import Foundation

enum LibraryRouter {
    case handleHeal(parameters: ParametersConvertible)
}
// MARK: - Endpoint
extension LibraryRouter: Endpoint {
    
    var baseURL: URL? { LibraryEnvironment.baseURL }
    
    var servicePath: String {
        switch self {
        case .handleHeal:
            return "/v2"
        }
    }
    
    var path: String {
        switch self {
        case .handleHeal:
            return "/transactions"
        }
    }
    
    var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        switch self {
        case .handleHeal:
            headers.add(.init(name: "content-type", value: "application/json"))
            if let token = UserDefaults.standard.string(forKey: "token"), let tokenType = UserDefaults.standard.string(forKey: "tokenType") {
                headers.add(.init(name: "Authorization", value: "\(tokenType) \(token)"))
            }
        }
        return headers
    }
    
    var method: HTTPMethod {
        switch self {
        case .handleHeal:
            return .post
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .handleHeal(let parameters):
            return parameters.asParameters
        }
    }
}
// MARK: - URLRequestConvertible
extension LibraryRouter: URLRequestConvertible {}

