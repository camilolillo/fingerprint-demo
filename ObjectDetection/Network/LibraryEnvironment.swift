//
//  LibraryEnvironment.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 17-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import Foundation

enum LibraryEnvironment {
    case prod
}

extension LibraryEnvironment {
    static var current: LibraryEnvironment = .prod
    static var baseURLAsString: String {
        switch current {
        case .prod:
            return "https://api.trust.lat/bio-identity-v2"
//            return "https://neloz.sa.ngrok.io"
        }
    }
    static var baseURL: URL? { .init(string: baseURLAsString) }
}
