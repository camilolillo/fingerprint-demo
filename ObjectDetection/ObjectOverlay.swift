//
//  ObjectOverlay.swift
//  ObjectDetection
//
//  Created by Camilo Lillo on 04-05-22.
//  Copyright © 2022 Y Media Labs. All rights reserved.
//

import UIKit

struct ObjectOverlay {
//    let name: String
    let originalBorderRect: CGRect
    let borderRect: CGRect
//    let nameStringSize: CGSize
    let color: UIColor
    let font: UIFont
    let confidenceValue: Int
}
